/**
 * The MIT License (MIT)
 * Copyright (c) 2017 Forfuture, LLC <we@forfuture.co.ke>
 */
import { Sequelize, DataTypes, Transaction } from "sequelize";
import * as migrate from "../lib";
/** Context passed to migration modules. */
export interface ISequelizeIntegrationContext {
    /** Sequelize instance. */
    sequelize: Sequelize;
    /** DB transaction. */
    transaction: Transaction;
}
/** Constructor options for `SequelizeIntegration`. */
export interface ISequelizeIntegrationOptions {
    /** Name for table used to store migration data. */
    tableName?: string;
}
/** Default construction options. */
export declare const defaultIntegrationOptions: ISequelizeIntegrationOptions;
/** DB model definition for a migration. */
export declare const MigrationModel: {
    id: {
        field: string;
        type: DataTypes.IntegerDataTypeConstructor;
        primaryKey: boolean;
        autoIncrement: boolean;
    };
    version: {
        field: string;
        type: DataTypes.TextDataTypeConstructor;
    };
    migratedAt: {
        field: string;
        type: DataTypes.DateDataTypeConstructor;
    };
};
/** Integration for Sequelize ORM. */
export declare class SequelizeIntegration {
    private sequelize;
    private options;
    private Model;
    private transaction;
    private isEmpty;
    constructor(sequelize: Sequelize, options?: ISequelizeIntegrationOptions);
    integrate(): Promise<migrate.project.IProjectState>;
    private close;
}
